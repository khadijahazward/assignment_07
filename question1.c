#include <stdio.h>
#include <string.h>
#define SIZE 100
void read(char a[]){
	printf("Enter a sentence to reverse:\n");
	fgets(a,SIZE,stdin);
}

void reverse(char a[]){
	int len =strlen(a);
	printf("\nThe reversed sentence is:");
	for(int i=len;i>=0;i--){
		printf("%c",a[i]);
	}
	printf("\n");
}
int main(){
	char name[SIZE];
	read(name);
	reverse(name);
}
