#include <stdio.h>

void read(int rows,int cols,int a[rows][cols]){
	printf("enter elements\n");
	for(int i = 0; i < rows; i++){
		for(int j = 0; j < cols; j++){
			scanf("%d", &a[i] [j]);
		}
	}
}

void add(int r,int c,int a[r][c],int b[r][c],int sum[r][c]){
	for(int i = 0; i<r; i++){
		for(int j = 0;j < c;j++){
			sum[i][j] = a[i][j] + b[i][j];
		}
	}
	
}
void multiply(int r, int c, int a[r][c],int b[r][c],int multiply[r][c]){
	for(int i = 0; i < r; i++){
		for(int j = 0; j < c;j++){
			for(int k = 0; k < c;k++){
				multiply[i][j] += a[i][k]*b[i][k];
			}
		}
	}
} 


void print(int rows,int cols,int c[rows][cols]){
	for(int i =  0; i<rows; i++){
		for(int j = 0; j<cols; j++){
			printf("%d ", c[i][j]);
			if(j == cols-1) printf("\n");
		}
	}

}


int main(){
	int rows,cols;
	int matrix1[100][100],matrix2[100][100],sum[100][100],multi[100][100];
	printf("Enter numbers of rows:\n");
	scanf("%d",&rows);
	printf("Enter numbers of columns:\n");
	scanf("%d",&cols);
	printf("--MATRIX 01--\n");
	read(rows,cols,matrix1);
	printf("--MATRIX 02--\n");
	read(rows,cols,matrix2);
	add(rows,cols,matrix1,matrix2,sum);
	multiply(rows,cols,matrix1,matrix2,multi);
	printf("\n--SUM OF TWO MATRIXES--\n");
	print(rows,cols,sum);
	printf("\n--MULTIPLICATION OF TWO MATRIXES--\n");
	print(rows,cols,multi);




}

